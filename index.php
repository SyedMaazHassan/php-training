<?php 

echo "Hello world!";

$items = [
    ['id' => 100, 'name' => 'product 1'],
    ['id' => 200, 'name' => 'product 2'],
    ['id' => 300, 'name' => 'product 3'],
    ['id' => 400, 'name' => 'product 4'],
];

$mapItems = array_map(function ($product) {
    return [
        'id' => $product['id'] * 2, 
        'name' => $product['name']
    ];
}, $items);

print_r($mapItems);
echo "<br>";
// Sorting problem
echo "<br>Associative array : Ascending order sort by value";
$array2=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); asort($array2);
echo "<ul>";
foreach($array2 as $y=>$y_value) {
    echo "<li>Age of ".$y." is : ".$y_value."</li>";
}
echo "</ul>";

echo "Associative array : Ascending order sort by Key";
$array3=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); ksort($array3);

echo "<ul>";
foreach($array3 as $y=>$y_value) { 
    echo "<li>Age of ".$y." is : ".$y_value."</li>";
}
echo "</ul>";

echo "Associative array : Descending order sorting by Value";
$age=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40");
arsort($age);

echo "<ul>";
foreach($age as $y=>$y_value){
    echo "<li>Age of ".$y." is : ".$y_value."</li>";
}
echo "</ul>";

echo "Associative array : Descending order sorting by Key";
echo "<ul>";
$array4=array("Sophia"=>"31","Jacob"=>"41","William"=>"39","Ramesh"=>"40"); krsort($array4);
foreach($array4 as $y=>$y_value) {
    echo "<li>Age of ".$y." is : ".$y_value."</li>";
} 
echo "</ul>";



function absolute_get_api($url, $parameters=array()){

    $opts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array(
                'Content-Type' => 'application/x-www-form-urlencoded',
                'api-key' => '78e35149-aa77-4ce7-8216-9aa8c5e21ace'
            ) 
        )
    );
    $context  = stream_context_create($opts);
  
    if ($parameters) {
        $parameter_string = "?".http_build_query($parameters);
        $api_url = $url.$parameter_string;  
    }else{
        $api_url = $url;
    }
    $result = file_get_contents($api_url, false, $context);
    return $result;
}

// $result = absolute_get_api("http://fizma.pythonanywhere.com/api/user/H9OCgGXRiwMYPJFwgYvFeVmybq23");


class Employee {
    public function __construct($name, $position, $salary) {
        $this->name = $name;
        $this->position = $position;
        $this->salary = $salary;
    }

    public function print_details(){
        return $this->name;
    }
}

class InputField {
    public function addConfiguration($id, $type, $label, $placeholder, $help_text=null, $default=null, $options=array()) {
        $this->id = $id;
        $this->type = $type;
        $this->label = $label;
        $this->placeholder = $placeholder;
        $this->help_text = ($help_text != null) ? $help_text : '';
        $this->default = ($default != null) ? $default : '';
        $this->options = $options;
    }

    public function render(){
        if ($this->type == 'checkbox') {
            return '
                <div class="form-group mb-3">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="'.$this->id.'" id="id_'.$this->id.'" value="checkedValue" class="custom-control-input" '.$this->default.'>
                        <span class="custom-control-indicator text-secondary">'.$this->label.'</span>
                    </label>
                </div>
            ';
        }else if ($this->type == "dropdown") {
            # code...
            $select_options = '';
            foreach ($this->options as $key => $value) {
                $single_option = '<option value="'.$key.'">'.$value.'</option>';
                $select_options .= $single_option;
            }

            return '
                <div class="form-group mb-3">
                    <label for="">'.$this->label.'</label>
                    <select class="form-select" name="'.$this->id.'" id="id_'.$this->id.'">
                        '.$select_options.'
                    </select>
                </div>
            ';            
        }

        return '
            <div class="form-group mb-3">
                <label for="">'.$this->label.'</label>
                <input type="'.$this->type.'" class="form-control" value="'.$this->default.'" name="'.$this->id.'" id="id_'.$this->id.'" placeholder="'.$this->placeholder.'">
                <small class="form-text text-muted">'.$this->help_text.'</small>
            </div>
        ';
    }
}

class Form extends InputField {
    public function __construct($head, $tagline, $input_fields_array) {
        $this->head = $head;
        $this->tagline = $tagline;
        $this->input_fields_array = $input_fields_array;
        $this->input_field_html = null;
        $this->form_html = null;
    }

    public function prepareInputFields() {
        $all_field_holder = '<div class="field-holder">';
        foreach ($this->input_fields_array as $field) {
            $this->addConfiguration(
                $field['id'],
                $field['type'],
                $field['label'],
                $field['placeholder'],
                $field['help_text'],
                $field['default'],
                $field['options']
            );
            $all_field_holder .= $this->render();
        }
        $all_field_holder .= "</div>";
        $this->input_field_html = $all_field_holder;
    }

    public function developForm() {
        $form_html = '<div class="border p-4">
                        <h2>'.$this->head.'</h2>
                        <p>'.$this->tagline.'</p>';
        $form_html .= $this->input_field_html;
        $form_html .= '</div>';
        $this->form_html = $form_html;
    }

    public function getHtml() {
        return $this->form_html;  
    }


}


$all_fields = array(
    array(
        'id' => 'email',
        'type' => 'email',
        'label' => 'Email',
        'placeholder' => null,
        'help_text' => 'Email should be active',
        'default' => '',
        'options' => array()
    ),

    array(
        'id' => 'first_name',
        'type' => 'text',
        'label' => 'First name',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

    array(
        'id' => 'last_name',
        'type' => 'text',
        'label' => 'Last name',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

    array(
        'id' => 'department',
        'type' => 'dropdown',
        'label' => 'Department',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array(
            'SE' => 'Software engineering',
            'MG' => 'Management',
            'PD' => 'Product development'
        )
    ),

    array(
        'id' => 'agreement',
        'type' => 'checkbox',
        'label' => 'I agree to the given privacy policy',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

);



$signupForm = new Form("Registration", "Form to start a new journey with us", $all_fields);
$signupForm->prepareInputFields();
$signupForm->developForm();

//    $this->id = $id;
//         $this->type = $type;
//         $this->label = $label;
//         $this->placeholder = $placeholder;
//         $this->help_text = ($help_text != null) ? $help_text : '';
//         $this->default = ($default != null) ? $default : '';
//         $this->options = $options;

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="row container py-5">
        <div class="col-sm-5">
            <div>
                <?php 
                    echo $signupForm->getHtml();
                ?>
            </div>
        </div>
    </div>
    

</body>
</html>