<?php 

class InputField {
    public function addConfiguration($id, $type, $label, $placeholder, $help_text=null, $default=null, $options=array()) {
        $this->id = $id;
        $this->type = $type;
        $this->label = $label;
        $this->placeholder = $placeholder;
        $this->help_text = ($help_text != null) ? $help_text : '';
        $this->default = ($default != null) ? $default : '';
        $this->options = $options;
    }

    public function render(){
        if ($this->type == 'checkbox') {
            return '
                <div class="form-group mb-3">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="'.$this->id.'" id="id_'.$this->id.'" value="checkedValue" class="custom-control-input" '.$this->default.'>
                        <span class="custom-control-indicator text-secondary">'.$this->label.'</span>
                    </label>
                </div>
            ';
        }else if ($this->type == "dropdown") {
            # code...
            $select_options = '';
            foreach ($this->options as $key => $value) {
                $single_option = '<option value="'.$key.'">'.$value.'</option>';
                $select_options .= $single_option;
            }

            return '
                <div class="form-group mb-3">
                    <label for="">'.$this->label.'</label>
                    <select class="form-select" name="'.$this->id.'" id="id_'.$this->id.'">
                        '.$select_options.'
                    </select>
                </div>
            ';            
        }

        return '
            <div class="form-group mb-3">
                <label for="">'.$this->label.'</label>
                <input type="'.$this->type.'" class="form-control" value="'.$this->default.'" name="'.$this->id.'" id="id_'.$this->id.'" placeholder="'.$this->placeholder.'">
                <small class="form-text text-muted">'.$this->help_text.'</small>
            </div>
        ';
    }
}

class Form extends InputField {
    public function __construct($head, $tagline, $input_fields_array, $btn_text) {
        $this->head = $head;
        $this->tagline = $tagline;
        $this->input_fields_array = $input_fields_array;
        $this->btn_text = $btn_text;
        $this->input_field_html = null;
        $this->form_html = null;
        $this->prepareInputFields();
        $this->developForm();
    }

    public function prepareInputFields() {
        $all_field_holder = '<div class="field-holder">';
        foreach ($this->input_fields_array as $field) {
            $this->addConfiguration(
                $field['id'],
                $field['type'],
                $field['label'],
                $field['placeholder'],
                $field['help_text'],
                $field['default'],
                $field['options']
            );
            $all_field_holder .= $this->render();
        }
        $all_field_holder .= "</div>";
        $this->input_field_html = $all_field_holder;
    }

    public function developForm() {
        $form_html = '<div class="border bg-white p-4 registration-form">
                        <form class="p-3">
                        <h2>'.$this->head.'</h2>
                        <p class="mb-4">'.$this->tagline.'</p>';
        $form_html .= $this->input_field_html;
        $form_html .= '<button type="submit" class="btn btn-primary px-4">'.$this->btn_text.'</button>';
        $form_html .= '</form></div>';
        $this->form_html = $form_html;
    }

    public function getHtml() {
        return $this->form_html;  
    }


}


$signup_form_fields = array(
    array(
        'id' => 'email',
        'type' => 'email',
        'label' => 'Email',
        'placeholder' => null,
        'help_text' => 'Email should be active',
        'default' => '',
        'options' => array()
    ),

    array(
        'id' => 'first_name',
        'type' => 'text',
        'label' => 'First name',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

    array(
        'id' => 'last_name',
        'type' => 'text',
        'label' => 'Last name',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

    array(
        'id' => 'password',
        'type' => 'password',
        'label' => 'Password',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),

    array(
        'id' => 'department',
        'type' => 'dropdown',
        'label' => 'Department',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array(
            '' => '',
            'SE' => 'Software engineering',
            'MG' => 'Management',
            'PD' => 'Product development'
        )
    ),

    array(
        'id' => 'agreement',
        'type' => 'checkbox',
        'label' => 'I agree to the given privacy policy',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    ),
);


$login_form_fields = array(
    array(
        'id' => 'email',
        'type' => 'email',
        'label' => 'Email',
        'placeholder' => null,
        'help_text' => '',
        'default' => '',
        'options' => array()
    ),
    array(
        'id' => 'password',
        'type' => 'password',
        'label' => 'Password',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array()
    )
);


$profile_update_fields = array(
    array(
        'id' => 'email',
        'type' => 'email',
        'label' => 'Email',
        'placeholder' => null,
        'help_text' => 'Email should be active',
        'default' => 'maaz@gmail.com',
        'options' => array()
    ),

    array(
        'id' => 'first_name',
        'type' => 'text',
        'label' => 'First name',
        'placeholder' => null,
        'help_text' => null,
        'default' => 'Syed Maaz',
        'options' => array()
    ),

    array(
        'id' => 'last_name',
        'type' => 'text',
        'label' => 'Last name',
        'placeholder' => null,
        'help_text' => null,
        'default' => 'Hassan',
        'options' => array()
    ),

    array(
        'id' => 'password',
        'type' => 'password',
        'label' => 'Password',
        'placeholder' => null,
        'help_text' => null,
        'default' => '123456789',
        'options' => array()
    ),

    array(
        'id' => 'department',
        'type' => 'dropdown',
        'label' => 'Department',
        'placeholder' => null,
        'help_text' => null,
        'default' => null,
        'options' => array(
            '' => '',
            'SE' => 'Software engineering',
            'MG' => 'Management',
            'PD' => 'Product development'
        )
    ),

    array(
        'id' => 'agreement',
        'type' => 'checkbox',
        'label' => 'I agree to the given privacy policy',
        'placeholder' => null,
        'help_text' => null,
        'default' => 'checked',
        'options' => array()
    ),
);




$signup_form = new Form("Registration", "Form to start a new journey with us", $signup_form_fields, 'Signup');
$login_form = new Form("Login", "Enter credentials to login", $login_form_fields, 'Login');
$profile_update_form = new Form("Update profile", "Update your profile update and save", $profile_update_fields, 'Save details');


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .registration-form{
            border-radius: 10px;
        }
    </style>
</head>
<body class="bg-light">
    <div class="container">
        <div class="row py-5">
            <div class="col-sm-12">
                <h2 class="mb-4">Dynamic form using PHP</h2>
            </div>
           
            <div class="col-sm-4">
                <div><?php echo $login_form->getHtml(); ?></div>
            </div>

            <div class="col-sm-4">
                <div><?php echo $signup_form->getHtml(); ?></div>
            </div>

            <div class="col-sm-4">
                <div><?php echo $profile_update_form->getHtml(); ?></div>
            </div>

        </div>
    </div>
    

</body>
</html>